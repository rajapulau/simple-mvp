package com.simple.mvp.view;

import com.simple.mvp.model.User;

import java.util.List;

public interface UserView {
    void onLoad(List<User> users);

    void onSave();

    void onDelete();

    void onUpdate();
}
