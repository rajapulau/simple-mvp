package com.simple.mvp.model;

public class User {
    private int id;
    private String nama;
    private String status;
    private String pekerjaan;
    private String noHp;

//    public User(int id, String nama, String status, String pekerjaan, String noHp) {
//        this.id = id;
//        this.nama = nama;
//        this.status = status;
//        this.pekerjaan = pekerjaan;
//        this.noHp = noHp;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }
}
