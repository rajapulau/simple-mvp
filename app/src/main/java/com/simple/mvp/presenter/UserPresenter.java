package com.simple.mvp.presenter;

import com.simple.mvp.model.User;

public interface UserPresenter {
    void save(User user);

    void update(User user);

    void delete(User user);

    void load();
}
