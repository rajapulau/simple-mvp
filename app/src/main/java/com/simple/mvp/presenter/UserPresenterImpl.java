package com.simple.mvp.presenter;

import com.simple.mvp.model.User;
import com.simple.mvp.view.UserView;

import java.util.ArrayList;
import java.util.List;

public class UserPresenterImpl implements UserPresenter {

    private UserView userView;
    private List<User> users = new ArrayList<>();
    private int no = 4;

    public UserPresenterImpl(UserView userView) {
        this.userView = userView;
    }

    private void init() {
        User user = new User();
        user.setId(1);
        user.setNama("Kristiani Adi");
        user.setNoHp("0853306841");
        user.setPekerjaan("Java Programmer");
        user.setStatus("Jomblo sejak lahir");
        users.add(user);

        User user1 = new User();
        user1.setId(2);
        user1.setNama("Bimo Joko");
        user1.setNoHp("085474748888");
        user1.setPekerjaan("IT Scurity");
        user1.setStatus("Udah punya pacar tapi drama melulu");
        users.add(user1);

        User user2 = new User();
        user2.setId(3);
        user2.setNama("Dirga");
        user2.setNoHp("085332423043");
        user2.setPekerjaan("IT Security");
        user2.setStatus("Udah punya pacar dan bentar lagi nikah");
        users.add(user2);
    }

    @Override
    public void save(User user) {
        no++;
        user.setId(no);
        users.add(user);

        userView.onSave();
    }

    @Override
    public void update(User user) {
        for (User model : users){
            if (model.getId() == user.getId()){
                model.setNama(user.getNama());
                model.setStatus(user.getStatus());
                model.setPekerjaan(user.getPekerjaan());
                model.setNoHp(user.getNoHp());

                break;
            }
        }

        userView.onUpdate();
    }

    @Override
    public void delete(User user) {
        users.remove(user);

        userView.onDelete();
    }

    @Override
    public void load() {
        userView.onLoad(users);
    }
}
