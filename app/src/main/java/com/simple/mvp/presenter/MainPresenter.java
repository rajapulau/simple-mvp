package com.simple.mvp.presenter;

/**
 * Created by rajapulau
 */

public interface MainPresenter {
    void hitungLuas(Double alas, Double tinggi);
}
