package com.simple.mvp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simple.mvp.R;
import com.simple.mvp.model.User;

import java.util.List;

public class MainRecyclerAdapter extends RecyclerView.Adapter<MainRecyclerAdapter.ViewHolder> {

    private List<User> users;
    private OnCallbackListener listener;

    public MainRecyclerAdapter(List<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_user, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        holder.textViewNama.setText(user.getNama());
        holder.textViewStatus.setText(user.getStatus());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setOnCallbackListener(OnCallbackListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView textViewNama;
        TextView textViewStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            textViewNama = itemView.findViewById(R.id.nama);
            textViewStatus = itemView.findViewById(R.id.status);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onClick(users.get(getAdapterPosition()));
            }
        }
    }

    public interface OnCallbackListener {

        void onClick(User user);
    }
}
